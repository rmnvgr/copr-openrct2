Name:			openrct2
Version:		0.3.0
Release:		1%{?dist}
Summary:		An open source re-implementation of RollerCoaster Tycoon 2
License:		GPLv3
URL:			https://openrct2.io/
Source0:		https://github.com/OpenRCT2/OpenRCT2/archive/v%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	SDL2-devel
BuildRequires:	freetype-devel
BuildRequires:	fontconfig-devel
BuildRequires:	libzip-devel
BuildRequires:	libpng-devel
BuildRequires:	speexdsp-devel
BuildRequires:	libcurl-devel
BuildRequires:	jansson-devel
BuildRequires:	openssl-devel
BuildRequires:	libicu-devel
BuildRequires:	zlib-devel
BuildRequires:	mesa-libGL-devel
BuildRequires:	duktape-devel

%description
OpenRCT2 is an open-source re-implementation of RollerCoaster Tycoon 2 (RCT2). The gameplay revolves around building and maintaining an amusement park containing attractions, shops and facilities. The player must try to make a profit and maintain a good park reputation whilst keeping the guests happy. OpenRCT2 allows for both scenario and sandbox play. Scenarios require the player to complete a certain objective in a set time limit whilst sandbox allows the player to build a more flexible park with optionally no restrictions or finance.

%prep
%autosetup -n OpenRCT2-%{version}

%build
mkdir build
cd build
%cmake -B . -DCMAKE_BUILD_TYPE=Release ..
%make_build

%install
cd build
%make_install

%files
%{_bindir}/%{name}
%{_bindir}/%{name}-cli
%{_datadir}/%{name}/*
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/applications/%{name}*.desktop
%{_datadir}/mime/packages/%{name}.xml
%{_docdir}/%{name}/*
%{_libdir}/libopenrct2.so
%{_mandir}/man6/%{name}.6.gz
%{_metainfodir}/%{name}.appdata.xml

%changelog
* Sat Aug 15 2020 - 0.3.0
- Upstream 0.3.0 release
- Add new dependencies
* Sat Apr 18 2020 - 0.2.6
- Upstream 0.2.6 release
* Wed Mar 25 2020 - 0.2.5
- Upstream 0.2.5 release
* Tue Oct 29 2019 - 0.2.4
- Upstream 0.2.4 release
* Sat Jul 20 2019 - 0.2.3-2
- Change cmake and make commands
* Thu Jul 11 2019 - 0.2.3-1
- Upstream 0.2.3 release
* Thu Mar 14 2019 - 0.2.2-1
- Upstream 0.2.2 release
* Tue Jan 29 2019 - 0.2.1-2
- Update CMake flags
* Sat Sep 15 2018 - 0.2.1-1
- Initial spec file
